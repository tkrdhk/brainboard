resource "azurerm_resource_group" "UBRG" {
  tags     = merge(var.tags, {})
  name     = "RGUB"
  location = "Japan East"
}

resource "azurerm_network_interface" "UBNI" {
  tags                = merge(var.tags, {})
  resource_group_name = azurerm_resource_group.UBRG.name
  name                = "NIUB"
  location            = "Japan East"

  ip_configuration {
    subnet_id                     = azurerm_subnet.UBSUB.id
    public_ip_address_id          = azurerm_public_ip.UBPIP.id
    private_ip_address_version    = "IPv4"
    private_ip_address_allocation = "Dynamic"
    primary                       = true
    name                          = "IPCONFIG2"
  }
}

resource "azurerm_network_security_group" "UBNSG" {
  tags                = merge(var.tags, {})
  resource_group_name = azurerm_resource_group.UBRG.name
  name                = "NSGUB"
  location            = "Japan East"

  security_rule {
    source_port_range          = "*"
    source_address_prefix      = "*"
    protocol                   = "Tcp"
    priority                   = 1001
    name                       = "SSH"
    direction                  = "Inbound"
    destination_port_range     = "22"
    destination_address_prefix = "*"
    description                = "for SSH"
    access                     = "Allow"
  }
}

resource "azurerm_public_ip" "UBPIP" {
  tags                = merge(var.tags, {})
  sku                 = "Standard"
  resource_group_name = azurerm_resource_group.UBRG.name
  name                = "PIPUB"
  location            = "Japan East"
  allocation_method   = "Static"
}

resource "azurerm_virtual_network" "UBVNET" {
  tags                = merge(var.tags, {})
  resource_group_name = azurerm_resource_group.UBRG.name
  name                = "VNETUB"
  location            = "Japan East"

  address_space = [
    "10.0.0.0/16",
  ]
}

resource "azurerm_subnet" "UBSUB" {
  virtual_network_name = azurerm_virtual_network.UBVNET.name
  resource_group_name  = azurerm_resource_group.UBRG.name
  name                 = "SUBUB"

  address_prefixes = [
    "10.0.1.0/24",
  ]
}

resource "azurerm_subnet_network_security_group_association" "NSGA" {
  subnet_id                 = azurerm_subnet.UBSUB.id
  network_security_group_id = azurerm_network_security_group.UBNSG.id
}

resource "azurerm_linux_virtual_machine" "UBVML" {
  tags                            = merge(var.tags, {})
  size                            = "Standard_B1s"
  resource_group_name             = azurerm_resource_group.UBRG.name
  name                            = "VMLUB"
  location                        = "Japan East"
  disable_password_authentication = true
  computer_name                   = "UB2004"
  admin_username                  = "dhika"
  admin_password                  = "Hosh!zor456"

  admin_ssh_key {
    username   = "dhika"
    public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQD1sKNcA0/Yv35Adk1jByqWivwWhjfBvMUnnqyvCmc2jGPQGOeA9lCIjukyZDXNutvB4gldOTSJ4FuWCw6IevawHTUEOAriBtTrdjBVZHGShrB5MTOf/DVNjVfFfn+xvzNduyRrKQCbNrVfqPn5Ou3OrfTY3J9yvVUb2cYcgPXmA5gE/L8GYxgTdubd9x5UTdUKL9rFbNwRpYpgnM2EIHlDURS0pwU1/tPJLJN+hI3XhfAGh68tR5o4CJwPA3Qt1YlEyF6xD5ihwL2WK2riGBi/sBN7iYvqrepBt487/cRzjCYPA/Noqv9AqKpKS2Mn+bOFF2BHWil301cw6oIqdse0/3+KpB6RpUiiIn4S8kTT3qX6C3WvgVi3lEFTd6MBxa4ZD3tx7sD+HXgMnRpxHeMlDL6i5IpgGupL46sf3SrTi+S8mjAynn5wfVu8bfRoKP02mdky+aDFvUCWhuMv4WeVRMKUiSvWNRvY/2zhC+5t4u/tbThvgG5dEgaBqE93zIU= generated-by-azure"
  }

  network_interface_ids = [
    azurerm_network_interface.UBNI.id,
  ]

  os_disk {
    storage_account_type = "Premium_LRS"
    name                 = "DiskVML_0"
    disk_size_gb         = 1024
    caching              = "None"
  }

  source_image_reference {
    version   = "latest"
    sku       = "18.04-LTS"
    publisher = "Canonical"
    offer     = "UbuntuServer"
  }
}

