variable "tags" {
  description = "Default tags to apply to all resources."
  type        = map(any)
  default     = { "archuuid" : "1169979f-9ff3-4454-acc5-03bc29636b33", "env" : "Test" }
}

